//
//  PixelCityOpenGLView.m
//  PixelTesterApp
//
//  Created by Mikael Eiman on 2009-05-05.
//  Copyright 2009 Electronic Magicians HB. All rights reserved.
//

#import "PixelCityOpenGLView.h"

#include "Win.h"
#include "glTypes.h"
#include "Render.h"

@implementation PixelCityOpenGLView

#define MyModuleName @"se.emage.PixelCitySaver"

-(void)prepare
{
  NSOpenGLContext* glContext = [self openGLContext];
  [glContext makeCurrentContext];
  // enable vsync
  GLint swap_val = 1;
  [glContext setValues:&swap_val forParameter:NSOpenGLCPSwapInterval];
  
  self.wantsBestResolutionOpenGLSurface = YES;
  worldState.view = (__bridge void *)self;
  AppInit(&worldState);
}


-(id)initWithCoder:(NSCoder*)coder
{
  self = [super initWithCoder:coder];
  
  [self prepare];
  
  return self;
}


- (id)initWithFrame:(NSRect)frame pixelFormat:(NSOpenGLPixelFormat*)pixelFormat 
{
  
  self = [super initWithFrame:frame pixelFormat:pixelFormat];
  if (self) {
    // Initialization code here.
       
  }
  
  return self;
}

- (id)initWithFrame:(NSRect)frame
{
  return [self initWithFrame:frame withTimer:YES];
}

- (id)initWithFrame:(NSRect)frame withTimer:(bool)withTimer
{
  NSOpenGLPixelFormatAttribute att[] = 
	{
		NSOpenGLPFAAllowOfflineRenderers,
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFAColorSize, (NSOpenGLPixelFormatAttribute)24,
		NSOpenGLPFAAlphaSize, (NSOpenGLPixelFormatAttribute)8,
		NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)24,
		NSOpenGLPFANoRecovery,
		NSOpenGLPFAAccelerated,
		(NSOpenGLPixelFormatAttribute)0
	};
  
  NSOpenGLPixelFormat *pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:att];
  
  self = [super initWithFrame:frame pixelFormat:pixelFormat];
  if (self) {
      // Initialization code here.
    [self prepare];
    
    if ( withTimer )
    {
      redrawTimer = [NSTimer scheduledTimerWithTimeInterval:1/60.0f target:self selector:@selector(update:) userInfo:nil repeats:YES];
    } else {
      redrawTimer = nil;
    }
  }
  return self;
}


-(void) dealloc
{
  NSLog(@"PixelCityOpenGLView dealloc");
  AppTerm(&worldState);
}

-(void)stop
{
  [redrawTimer invalidate];
}

-(void)reshape
{
  [super reshape];
  RenderResize(&worldState);
}

-(void)update:(void*)data
{
  [self setNeedsDisplay:YES];
}


- (void)drawRect:(NSRect)rect 
{
  NSOpenGLContext* glContext = [self openGLContext];
  [glContext makeCurrentContext];

  AppUpdate(&worldState);
//  glFinish();
  [glContext flushBuffer];
  
}


- (void)viewDidMoveToWindow
{
  // The initial move to a window may alter the viewport:
  [super viewDidMoveToWindow];
  RenderResize(&worldState);
}

- (void)viewWillMoveToWindow:(NSWindow *)newWindow
{
  state_t *weakWorldState = &worldState;
  
  [super viewWillMoveToWindow:newWindow];
  
  // In the unlikely event that we change screens, we need to alter the viewport, because the backing scale factor may change from screen to screen:
  if (moveToScreenNotification)
    [[NSNotificationCenter defaultCenter] removeObserver:moveToScreenNotification];
  
  if (newWindow)
  {
    moveToScreenNotification = [[NSNotificationCenter defaultCenter] addObserverForName:NSWindowDidChangeScreenNotification object:newWindow queue:nil usingBlock:^(NSNotification * _Nonnull note) {
      RenderResize(weakWorldState);
    }];
  }
  else
  {
    [redrawTimer invalidate];
  }
}

@end

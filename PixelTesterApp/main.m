//
//  main.m
//  PixelTesterApp
//
//  Created by Mikael Eiman on 2009-05-05.
//  Copyright Electronic Magicians HB 2009. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}

#include "State.h"

bool  RenderBloom (state_t* state);
void  RenderEffectCycle (state_t* state);
bool  RenderFlat (state_t* state);
void  RenderFlatToggle (state_t* state);
float RenderFogDistance (state_t* state);
bool  RenderFog (state_t* state);
void  RenderFogToggle (state_t* state);
void  RenderFogFX (state_t* state, float scalar);
void  RenderFPSToggle (state_t* state);
void  RenderInit (state_t* state);
void  RenderLetterboxToggle (state_t* state);
int   RenderMaxTextureSize(state_t* state);
void  RenderResize (state_t* state);
void  RenderTerm (state_t* state);
void  RenderUpdate (state_t* state);
bool  RenderWireframe (state_t* state);
void  RenderWireframeToggle (state_t* state);
void  RenderHelpToggle (state_t* state);
void  RenderPrint (state_t* state, int x, int y, int font, GLrgba color, const char *fmt, ...);
void  RenderPrint (state_t* state, int line, const char *fmt, ...);






/*-----------------------------------------------------------------------------

  Camera.cpp

  2009 Shamus Young

-------------------------------------------------------------------------------

  This tracks the position and oritentation of the camera. In screensaver 
  mode, it moves the camera around the world in order to create dramatic 
  views of the hot zone.  

-----------------------------------------------------------------------------*/

#define EYE_HEIGHT              2.0f
#define MAX_PITCH               85
#define FLYCAM_CIRCUT           60000
#define FLYCAM_CIRCUT_HALF      (FLYCAM_CIRCUT / 2)
#define FLYCAM_LEG              (FLYCAM_CIRCUT / 4)
#define ONE_SECOND              1000
#define CAMERA_CHANGE_INTERVAL  15
#define CAMERA_CYCLE_LENGTH     (CAMERA_MODES*CAMERA_CHANGE_INTERVAL)

#ifdef _WIN32
#include <windows.h>
#endif
#include <math.h>
#include <time.h>

#include "glTypes.h"
#include "Ini.h"
#include "Macro.h"
#include "PixelMath.h"
#include "World.h"
#include "Win.h"


enum
{
  CAMERA_FLYCAM1,
  CAMERA_ORBIT_INWARD,
  CAMERA_ORBIT_OUTWARD,
  CAMERA_ORBIT_ELLIPTICAL,
  CAMERA_FLYCAM2,
  CAMERA_SPEED,
  CAMERA_SPIN,
  CAMERA_FLYCAM3,
  CAMERA_MODES
};


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/


static GLvector flycam_position (state_t* state, unsigned t)
{

  unsigned    leg;
  float       delta;
  GLvector    start, end;
  GLbbox      hot_zone;

  hot_zone = WorldHotZone (state);
  t %= FLYCAM_CIRCUT; 
  leg = t / FLYCAM_LEG;
  delta = (float)(t % FLYCAM_LEG) / FLYCAM_LEG;
  switch (leg) {
  case 0:
    start = glVector (hot_zone.min.x, 25.0f, hot_zone.min.z);
    end = glVector (hot_zone.min.x, 60.0f, hot_zone.max.z);
    break;
  case 1:
    start = glVector (hot_zone.min.x, 60.0f, hot_zone.max.z);
    end = glVector (hot_zone.max.x, 25.0f, hot_zone.max.z);
    break;
  case 2:
    start = glVector (hot_zone.max.x, 25.0f, hot_zone.max.z);
    end = glVector (hot_zone.max.x, 60.0f, hot_zone.min.z);
    break;
  case 3:
    start = glVector (hot_zone.max.x, 60.0f, hot_zone.min.z);
    end = glVector (hot_zone.min.x, 25.0f, hot_zone.min.z);
    break;
  }
  delta = MathScalarCurve (delta);
  return glVectorInterpolate (start, end, delta);


}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

static void do_auto_cam (state_t* state)
{

  float     dist;
  unsigned  t;
  unsigned  elapsed;
  unsigned  now;
  int       behavior; 
  GLvector  target;

  now = GetTickCount ();
  elapsed = now - state->camera.last_update;
  elapsed = MIN (elapsed, 50); //limit to 1/20th second worth of time
  if (elapsed == 0)
    return;
  state->camera.last_update = now;
  t = time (NULL) % CAMERA_CYCLE_LENGTH;
#if SCREENSAVER
  behavior = t / CAMERA_CHANGE_INTERVAL;
#else
  behavior = camera_behavior;
#endif
  state->camera.tracker += (float)elapsed / 300.0f;
  //behavior = CAMERA_FLYCAM1; 
  switch (behavior) {
  case CAMERA_ORBIT_INWARD:
    state->camera.auto_position.x = WORLD_HALF + sinf (state->camera.tracker * DEGREES_TO_RADIANS) * 150.0f;
    state->camera.auto_position.y = 60.0f;
    state->camera.auto_position.z = WORLD_HALF + cosf (state->camera.tracker * DEGREES_TO_RADIANS) * 150.0f;
    target = glVector (WORLD_HALF, 40.0f, WORLD_HALF);
    break;
  case CAMERA_ORBIT_OUTWARD:
    state->camera.auto_position.x = WORLD_HALF + sinf (state->camera.tracker * DEGREES_TO_RADIANS) * 250.0f;
    state->camera.auto_position.y = 60.0f;
    state->camera.auto_position.z = WORLD_HALF + cosf (state->camera.tracker * DEGREES_TO_RADIANS) * 250.0f;
    target = glVector (WORLD_HALF, 30.0f, WORLD_HALF);
    break;
  case CAMERA_ORBIT_ELLIPTICAL:
    dist = 150.0f + sinf (state->camera.tracker * DEGREES_TO_RADIANS / 1.1f) * 50;
    state->camera.auto_position.x = WORLD_HALF + sinf (state->camera.tracker * DEGREES_TO_RADIANS) * dist;
    state->camera.auto_position.y = 60.0f;
    state->camera.auto_position.z = WORLD_HALF + cosf (state->camera.tracker * DEGREES_TO_RADIANS) * dist;
    target = glVector (WORLD_HALF, 50.0f, WORLD_HALF);
    break;
  case CAMERA_FLYCAM1:
  case CAMERA_FLYCAM2:
  case CAMERA_FLYCAM3:
    state->camera.auto_position = (flycam_position (state, now) + flycam_position (state, now + 4000)) / 2.0f;
    target = flycam_position (state, now + FLYCAM_CIRCUT_HALF - ONE_SECOND * 3);
    break;
  case CAMERA_SPEED:
    state->camera.auto_position = (flycam_position (state, now) + flycam_position (state, now + 500)) / 2.0f;
    target = flycam_position (state, now + ONE_SECOND * 5);
    state->camera.auto_position.y /= 2;
    target.y /= 2;
    break;
  case CAMERA_SPIN:
  default:  
    target.x = WORLD_HALF + sinf (state->camera.tracker * DEGREES_TO_RADIANS) * 300.0f;
    target.y = 30.0f;
    target.z = WORLD_HALF + cosf (state->camera.tracker * DEGREES_TO_RADIANS) * 300.0f;
    state->camera.auto_position.x = WORLD_HALF + sinf (state->camera.tracker * DEGREES_TO_RADIANS) * 50.0f;
    state->camera.auto_position.y = 60.0f;
    state->camera.auto_position.z = WORLD_HALF + cosf (state->camera.tracker * DEGREES_TO_RADIANS) * 50.0f;
  }
  dist = MathDistance (state->camera.auto_position.x, state->camera.auto_position.z, target.x, target.z);
  state->camera.auto_angle.y = MathAngle1 (-MathAngle4 (state->camera.auto_position.x, state->camera.auto_position.z, target.x, target.z));
  state->camera.auto_angle.x = 90.0f + MathAngle4 (0, state->camera.auto_position.y, dist, target.y);

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraAutoToggle (state_t* state)
{

  state->camera.cam_auto = !state->camera.cam_auto;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraNextBehavior (state_t* state)
{

  state->camera.camera_behavior++;
  state->camera.camera_behavior %= CAMERA_MODES;

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraYaw (state_t* state, float delta)
{

  state->camera.moving = true;
  state->camera.angle.y -= delta;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraPitch (state_t* state, float delta)
{

  state->camera.moving = true;
  state->camera.angle.x -= delta;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraPan (state_t* state, float delta)
{

  float           move_x, move_y;

  state->camera.moving = true;
  move_x = (float)sin (-state->camera.angle.y * DEGREES_TO_RADIANS) / 10.0f;
  move_y = (float)cos (-state->camera.angle.y * DEGREES_TO_RADIANS) / 10.0f;
  state->camera.position.x -= move_y * delta;
  state->camera.position.z -= -move_x * delta;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraForward (state_t* state, float delta)
{

  float           move_x, move_y;

  state->camera.moving = true;
  move_y = (float)sin (-state->camera.angle.y * DEGREES_TO_RADIANS) / 10.0f;
  move_x = (float)cos (-state->camera.angle.y * DEGREES_TO_RADIANS) / 10.0f;
  state->camera.position.x -= move_y * delta;
  state->camera.position.z -= move_x * delta;

}



/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraSelectionPitch (float delta)
{

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraSelectionZoom (float delta)
{

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraSelectionYaw (float delta)
{

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

GLvector CameraPosition (state_t* state)		
{
 
  if (state->camera.cam_auto)
    return state->camera.auto_position;
  return state->camera.position;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraReset (state_t* state)		
{

  state->camera.position.y = 50.0f;
  state->camera.position.x = WORLD_HALF;
  state->camera.position.z = WORLD_HALF;
  state->camera.angle.x = 0.0f;
  state->camera.angle.y = 0.0f;
  state->camera.angle.z = 0.0f;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraPositionSet (state_t* state, GLvector new_pos)		
{

  state->camera.position = new_pos;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

GLvector CameraAngle (state_t* state)		
{

  if (state->camera.cam_auto)
    return state->camera.auto_angle;
  return state->camera.angle;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraAngleSet (state_t* state, GLvector new_angle)		
{

  state->camera.angle = new_angle;
  state->camera.angle.x = CLAMP (state->camera.angle.x, -80.0f, 80.0f);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraInit (state_t* state)		
{

  state->camera.angle = IniVector ("CameraAngle");
  state->camera.position = IniVector ("CameraPosition");

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraUpdate (state_t* state)		
{

#if SCREENSAVER
  state->camera.cam_auto = true;
#endif
  if (state->camera.cam_auto) 
    do_auto_cam (state);
  if (state->camera.moving) 
    state->camera.movement *= 1.1f;
  else
    state->camera.movement = 0.0f;
  state->camera.movement = CLAMP (state->camera.movement, 0.01f, 1.0f);
  
  if (state->camera.angle.y < 0.0f) 
    state->camera.angle.y = 360.0f - (float)fmod (fabs (state->camera.angle.y), 360.0f);
  state->camera.angle.y = (float)fmod (state->camera.angle.y, 360.0f);
  state->camera.angle.x = CLAMP (state->camera.angle.x, -MAX_PITCH, MAX_PITCH);
  state->camera.moving = false;
 
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void CameraTerm (state_t* state)		
{

  //just store our most recent position in the ini
  IniVectorSet ("CameraAngle", state->camera.angle);
  IniVectorSet ("CameraPosition", state->camera.position);
 
}

/*-----------------------------------------------------------------------------
									               r a n d o m
-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  The Mersenne Twister by Matsumoto and Nishimura <matumoto@math.keio.ac.jp>.
  It sets new standards for the period, quality and speed of random number
  generators. The incredible period is 2^19937 - 1, a number with about 6000
  digits; the 32-bit random numbers exhibit best possible equidistribution
  properties in dimensions up to 623; and it's fast, very fast. 
-----------------------------------------------------------------------------*/


#define LOWER_MASK            0x7fffffff 
#define M                     397
#define MATRIX_A              0x9908b0df 
#define N                     624
#define TEMPERING_MASK_B      0x9d2c5680
#define TEMPERING_MASK_C      0xefc60000
#define TEMPERING_SHIFT_L(y)  (y >> 18)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define UPPER_MASK            0x80000000 

#include <memory.h>
#include "Random.h"

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

unsigned long RandomVal (state_t* state)
{

  int		            kk;
  unsigned long	    y;
  
  if (state->random.k == N) {
    for (kk = 0; kk < N - M; kk++) {
      y = (state->random.ptgfsr[kk] & UPPER_MASK) | (state->random.ptgfsr[kk + 1] & LOWER_MASK);
      state->random.ptgfsr[kk] = state->random.ptgfsr[kk + M] ^ (y >> 1) ^ state->random.mag01[y & 0x1];
      }
    for (; kk < N - 1; kk++) {
      y = (state->random.ptgfsr[kk] & UPPER_MASK) | (state->random.ptgfsr[kk + 1] & LOWER_MASK);
      state->random.ptgfsr[kk] = state->random.ptgfsr[kk + (M - N)] ^ (y >> 1) ^ state->random.mag01[y & 0x1];
      }
    y = (state->random.ptgfsr[N - 1] & UPPER_MASK) | (state->random.ptgfsr[0] & LOWER_MASK);
    state->random.ptgfsr[N - 1] = state->random.ptgfsr[M - 1] ^ (y >> 1) ^ state->random.mag01[y & 0x1];
    state->random.k = 0;
    }
  y = state->random.ptgfsr[state->random.k++];
  y ^= TEMPERING_SHIFT_U (y);
  y ^= TEMPERING_SHIFT_S (y) & TEMPERING_MASK_B;
  y ^= TEMPERING_SHIFT_T (y) & TEMPERING_MASK_C;
  return y ^= TEMPERING_SHIFT_L (y);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

unsigned long RandomVal (state_t* state, int range)
{

  return range ? (RandomVal (state) % range) : 0;

}

/*-----------------------------------------------------------------------------

static int              k = 1;
static unsigned long    mag01[2] = {0x0, MATRIX_A};
static unsigned long    ptgfsr[N];

-----------------------------------------------------------------------------*/

void RandomInit (state_t* state, unsigned long seed)
{

  //int	    k;
  
  //memset (ptgfsr, 0, sizeof (ptgfsr));
  //mag01[0] = 0x0;
  //mag01[1] = MATRIX_A;
  state->random.ptgfsr[0] = seed;
  for (state->random.k = 1; state->random.k < N; state->random.k++)
    state->random.ptgfsr[state->random.k] = 69069 * state->random.ptgfsr[state->random.k - 1];
  state->random.k = 1;

}


/*-----------------------------------------------------------------------------

  Ini.cpp

  2009 Shamus Young


-------------------------------------------------------------------------------
  
  This takes various types of data and dumps them into a predefined ini file.

-----------------------------------------------------------------------------*/

#import <ScreenSaver/ScreenSaver.h>
#define MyModuleName @"se.emage.PixelCitySaver"

#define FORMAT_VECTOR       "%f %f %f"
#define MAX_RESULT          256
#define FORMAT_FLOAT        "%1.2f"
#define INI_FILE            ".\\" APP ".ini"
#define SECTION             "Settings"

#ifdef _WIN32
#include <windows.h>
#else
#include <stdlib.h>
#include <string.h>
#endif
#include <stdio.h>
#include "glTypes.h"

#include "Ini.h"
#include "Win.h"

#ifdef _WIN32

static char                 result[MAX_RESULT];

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

int IniInt (char* entry)
{

  int         result;

  result = GetPrivateProfileInt (SECTION, entry, 0, INI_FILE);
  return result;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void IniIntSet (char* entry, int val)
{

  char        buf[20];

  sprintf (buf, "%d", val);
  WritePrivateProfileString (SECTION, entry, buf, INI_FILE);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

float IniFloat (char* entry)
{

  float     f;

  GetPrivateProfileString (SECTION, entry, "", result, MAX_RESULT, INI_FILE);
  f = (float)atof (result);
  return f;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void IniFloatSet (char* entry, float val)
{

  char        buf[20];
  
  sprintf (buf, FORMAT_FLOAT, val);
  WritePrivateProfileString (SECTION, entry, buf, INI_FILE);

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

char* IniString (char* entry)
{

  GetPrivateProfileString (SECTION, entry, "", result, MAX_RESULT, INI_FILE);
  return result;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void IniStringSet (char* entry, char* val)
{

  WritePrivateProfileString (SECTION, entry, val, INI_FILE);

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void IniVectorSet (char* entry, GLvector v)
{
  
  sprintf (result, FORMAT_VECTOR, v.x, v.y, v.z);
  WritePrivateProfileString (SECTION, entry, result, INI_FILE);

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

GLvector IniVector (char* entry)
{

  GLvector  v;

  v.x = v.y = v.z = 0.0f;
  GetPrivateProfileString (SECTION, entry, "0 0 0", result, MAX_RESULT, INI_FILE);
  sscanf (result, FORMAT_VECTOR, &v.x, &v.y, &v.z);
  return v;

}

#endif // _WIN32

#ifdef __APPLE__

GLvector IniVector(char* entry)
{
  GLvector v;
  
  v.x = v.y = v.z = 0.0f;
  
  if ( strcmp(entry, "CameraAngle") == 0 ) {
  }
  if ( strcmp(entry, "CameraPosition") == 0 ) {
  }
  
  return v;
}


int IniInt(char* entry)
{
  ScreenSaverDefaults* defaults = [ScreenSaverDefaults defaultsForModuleWithName:MyModuleName];
  
  if ( strcmp(entry, "Letterbox") == 0 ) {
    return [defaults boolForKey:@"Letterbox"];
  }
  if ( strcmp(entry, "Wireframe") == 0 ) {
    return [defaults boolForKey:@"Wireframe"];
  }
  if ( strcmp(entry, "ShowFPS") == 0 ) {
    return 0;
  }
  if ( strcmp(entry, "ShowFog") == 0 ) {
    return [defaults boolForKey:@"Fog"];
  }
  if ( strcmp(entry, "Effect") == 0 ) {
    return [defaults boolForKey:@"Bloom"];
  }
  if ( strcmp(entry, "Flat") == 0 ) {
    return [defaults boolForKey:@"Flat"];
  }
  if ( strcmp(entry, "SetDefaults") == 0 ) {
    return 1;
  }
  
  return 0;
}

void IniIntSet(char* entry, int val)
{
}


void IniVectorSet(char* entry, GLvector val)
{
}

#endif


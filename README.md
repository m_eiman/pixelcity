# Pixel City Saver OSX

Original app by Shamus Young, OSX adaptation by Mikael Eiman.
Licensed under GPLv3.

## What is it?

A screen saver that shows a procedurally generated city nightscape.
Shamus Young wrote it as a small project, documented on his blog:
http://www.shamusyoung.com/twentysidedtale/?p=2940

## OSX port?

I thought it was a nifty thing that looked nice, so when Shamus released
the source I gave porting it a try. Turned out it wasn't too complicated,
and you have the result here.
  
Things I've done:
  
* Wrote some screensaver shell code
* Ported the Win32-specifics to Cocoa
* Moved all the statics to some structs to allow for multiple screens
  
The source is included, and is also an awful hack. But it works for
me, so it's probably flawless... ;)
  
I've included changes from the official Subversion repo up to r28.
You can find it at http://code.google.com/p/pixelcity/

In addition to the inital port, fixes to make it work again in 10.6
were done by Dave Warker, and fixes to make it work with Catalina
were done by Nick Zitzmann. Thanks!

Source code is now available in git on Bitbucket:
https://bitbucket.org/m_eiman/pixelcity/

## Issues?

There are a few things that could be improved:
  
* The preview doesn't apply changed settings directly

## Contact!
  
If there's anything you'd like to say about the OSX specifics, feel
free to send an email to mikael@eiman.tv. For stuff that's not about
OSX, it's probably better to contact Shamus through his blog.

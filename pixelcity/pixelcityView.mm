//
//  pixelcityView.m
//  pixelcity
//
//  Created by Mikael Eiman on 2009-05-05.
//  Copyright (c) 2009, Electronic Magicians HB. All rights reserved.
//

#import "pixelcityView.h"
#import "PixelCityOpenGLView.h"


@implementation pixelcityView

#define MyModuleName @"se.emage.PixelCitySaver"

- (id)initWithFrame:(NSRect)frame isPreview:(BOOL)isPreview
{
  self = [super initWithFrame:frame isPreview:isPreview];
  
  if (self) 
  {
    ScreenSaverDefaults *defaults = [ScreenSaverDefaults defaultsForModuleWithName:MyModuleName]; 
    // Register our default values 
    [defaults registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys: 
      @"NO", @"Letterbox", 
      @"NO", @"Flat", 
      @"YES", @"Bloom", 
      @"NO", @"Wireframe", 
      @"YES", @"Fog", 
      nil]
    ];   
    
    [self setAnimationTimeInterval:1/60.0];
    
    [self addSubview:[[PixelCityOpenGLView alloc] initWithFrame:[self bounds]]];
  }
  
  return self;
}

- (void)startAnimation
{
  [super startAnimation];
}

- (void)stopAnimation
{
    [super stopAnimation];
}

- (void)drawRect:(NSRect)rect
{
  [super drawRect:rect];
}

- (void)animateOneFrame
{
  return;
}

- (BOOL)hasConfigureSheet
{
  return YES;
}

- (NSWindow*)configureSheet
{

  ScreenSaverDefaults* defaults = [ScreenSaverDefaults defaultsForModuleWithName:MyModuleName];
  
  if (!configSheet) 
  { 
    if (![NSBundle loadNibNamed:@"ConfigSheet" owner:self]) 
    { 
      NSLog( @"Failed to load configure sheet." ); 
      NSBeep(); 
    } 
  } 
  
  [letterboxOption setState:[defaults boolForKey:@"Letterbox"]];
  [flatOption setState:[defaults boolForKey:@"Flat"]];
  [bloomOption setState:[defaults boolForKey:@"Bloom"]];
  [wireframeOption setState:[defaults boolForKey:@"Wireframe"]];
  [fogOption setState:[defaults boolForKey:@"Fog"]];
  
  return configSheet; 
}


- (IBAction)cancelClick:(id)sender 
{ 
  [[NSApplication sharedApplication] endSheet:configSheet]; 
}

- (IBAction) okClick: (id)sender 
{ 
  ScreenSaverDefaults *defaults; 
  defaults = [ScreenSaverDefaults defaultsForModuleWithName:MyModuleName]; 
  // Update our defaults 
  [defaults setBool:[letterboxOption state] forKey:@"Letterbox"]; 
  [defaults setBool:[flatOption state] forKey:@"Flat"]; 
  [defaults setBool:[bloomOption state] forKey:@"Bloom"]; 
  [defaults setBool:[wireframeOption state] forKey:@"Wireframe"]; 
  [defaults setBool:[fogOption state] forKey:@"Fog"]; 
  // Save the settings to disk 
  [defaults synchronize]; 
  // Close the sheet 
  [[NSApplication sharedApplication] endSheet:configSheet]; 
}

@end

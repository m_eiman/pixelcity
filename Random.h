
#include "State.h"

#define COIN_FLIP     (RandomVal (_state, 2) == 0)

unsigned long RandomVal (state_t* state, int range);
unsigned long RandomVal (state_t* state);
void          RandomInit (state_t* state, unsigned long seed);

#include "State.h"

class CLight
{
  GLvector        _position;
  GLrgba          _color;
  int             _size;
  float           _vert_size;
  float           _flat_size;
  bool            _blink;
  unsigned        _blink_interval;
  int             _cell_x;
  int             _cell_z;
  state_t*        _state;
  
public:
                  CLight(state_t* state, GLvector pos, GLrgba color, int size);
  class CLight*   _next;
  void            Render ();
  void            Blink ();

};

void  LightRender (state_t* state);
void  LightClear (state_t* state);
int   LightCount (state_t* state);

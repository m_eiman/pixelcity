
#include "State.h"

GLrgba    WorldBloomColor (state_t* state);
char      WorldCell (state_t* state, int x, int y);
GLrgba    WorldLightColor (state_t* state, unsigned index);
int       WorldLogoIndex (state_t* state);
GLbbox    WorldHotZone (state_t* state);
void      WorldInit (state_t* state);
float     WorldFade (state_t* state);
void      WorldRender (state_t* state);
void      WorldReset (state_t* state);
int       WorldSceneBegin (state_t* state);
int       WorldSceneElapsed (state_t* state);
void      WorldTerm (state_t* state);
void      WorldUpdate (state_t* state);



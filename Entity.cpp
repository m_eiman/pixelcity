/*-----------------------------------------------------------------------------

  Entity.cpp

  Copyright (c) 2005 Shamus Young
  All Rights Reserved

-------------------------------------------------------------------------------

  An entity is any renderable stationary object in the world.  This is an 
  abstract class.  This module gathers up the Entities, sorts them by 
  texture use and location, and then stores them in OpenGL render lists
  for faster rendering.    

-----------------------------------------------------------------------------*/

#ifdef _WIN32
#include <windows.h>
#include <gl\gl.h>
#else
#include <GLUT/GLUT.h>
#include <stdlib.h>
#endif
#include <math.h>

#include "Camera.h"
#include "Entity.h"
#include "Macro.h"
#include "PixelMath.h"
#include "Render.h"
#include "Texture.h"
#include "World.h"
#include "Visible.h"
#include "Win.h"


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

static int do_compare (const void *arg1, const void *arg2 )
{

  struct entity*  e1 = (struct entity*)arg1;
  struct entity*  e2 = (struct entity*)arg2;

  if (e1->object->Alpha () && !e2->object->Alpha ())
    return 1;
  if (!e1->object->Alpha () && e2->object->Alpha ())
    return -1;
  if (e1->object->Texture () > e2->object->Texture ())
    return 1;
  else if (e1->object->Texture () < e2->object->Texture ())
    return -1;
  return 0;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void add (state_t* state, CEntity* b)
{

  state->entity.entity_list = (entity*)realloc (state->entity.entity_list, sizeof (entity) * (state->entity.entity_count + 1));
  state->entity.entity_list[state->entity.entity_count].object = b;
  state->entity.entity_count++;
  state->entity.polycount = 0;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

static void do_compile (state_t* state)
{

  int       i;
  int       x, y;

  if (state->entity.compiled)
    return;
  x = state->entity.compile_x;
  y = state->entity.compile_y;
  //Changing textures is pretty expensive, and thus sorting the entites so that
  //they are grouped by texture used can really improve framerate.
  //qsort (entity_list, entity_count, sizeof (struct entity), do_compare);
  //sorted = true;
  //Now group entites on the grid 
  //make a list for the textured objects in this region
  if (!state->entity.cell_list[x][y].list_textured)
    state->entity.cell_list[x][y].list_textured = glGenLists(1);
  glNewList (state->entity.cell_list[x][y].list_textured, GL_COMPILE);
  state->entity.cell_list[x][y].pos = glVector (GRID_TO_WORLD(x), 0.0f, (float)y * GRID_RESOLUTION);
  for (i = 0; i < state->entity.entity_count; i++) {
    GLvector pos = state->entity.entity_list[i].object->Center ();
    if (WORLD_TO_GRID(pos.x) == x && WORLD_TO_GRID(pos.z) == y && !state->entity.entity_list[i].object->Alpha ()) {
      glBindTexture(GL_TEXTURE_2D, state->entity.entity_list[i].object->Texture ());
      state->entity.entity_list[i].object->Render ();
    }
  }
  glEndList();	

  //Make a list of flat-color stuff (A/C units, ledges, roofs, etc.)
  if (!state->entity.cell_list[x][y].list_flat)
    state->entity.cell_list[x][y].list_flat = glGenLists(1);
  glNewList (state->entity.cell_list[x][y].list_flat, GL_COMPILE);
  glEnable (GL_CULL_FACE);
  state->entity.cell_list[x][y].pos = glVector (GRID_TO_WORLD(x), 0.0f, (float)y * GRID_RESOLUTION);
  for (i = 0; i < state->entity.entity_count; i++) {
    GLvector pos = state->entity.entity_list[i].object->Center ();
    if (WORLD_TO_GRID(pos.x) == x && WORLD_TO_GRID(pos.z) == y && !state->entity.entity_list[i].object->Alpha ()) {
      state->entity.entity_list[i].object->RenderFlat (false);
    }
  }
  glEndList();	
  //Now a list of flat-colored stuff that will be wireframe friendly
  if (!state->entity.cell_list[x][y].list_flat_wireframe)
    state->entity.cell_list[x][y].list_flat_wireframe = glGenLists(1);
  glNewList (state->entity.cell_list[x][y].list_flat_wireframe, GL_COMPILE);
  glEnable (GL_CULL_FACE);
  state->entity.cell_list[x][y].pos = glVector (GRID_TO_WORLD(x), 0.0f, (float)y * GRID_RESOLUTION);
  for (i = 0; i < state->entity.entity_count; i++) {
    GLvector pos = state->entity.entity_list[i].object->Center ();
    if (WORLD_TO_GRID(pos.x) == x && WORLD_TO_GRID(pos.z) == y && !state->entity.entity_list[i].object->Alpha ()) {
      state->entity.entity_list[i].object->RenderFlat (true);
    }
  }
  glEndList();	
  //Now a list of stuff to be alpha-blended, and thus rendered last
  if (!state->entity.cell_list[x][y].list_alpha)
    state->entity.cell_list[x][y].list_alpha = glGenLists(1);
  glNewList (state->entity.cell_list[x][y].list_alpha, GL_COMPILE);
  state->entity.cell_list[x][y].pos = glVector (GRID_TO_WORLD(x), 0.0f, (float)y * GRID_RESOLUTION);
  glDepthMask (false);
  glEnable (GL_BLEND);
  glDisable (GL_CULL_FACE);
  for (i = 0; i < state->entity.entity_count; i++) {
    GLvector pos = state->entity.entity_list[i].object->Center ();
    if (WORLD_TO_GRID(pos.x) == x && WORLD_TO_GRID(pos.z) == y && state->entity.entity_list[i].object->Alpha ()) {
      glBindTexture(GL_TEXTURE_2D, state->entity.entity_list[i].object->Texture ());
      state->entity.entity_list[i].object->Render ();
    }
  }
  glDepthMask (true);
  glEndList();	

  //now walk the grid
  state->entity.compile_x++;
  if (state->entity.compile_x == GRID_SIZE) {
    state->entity.compile_x = 0;
    state->entity.compile_y++;
    if (state->entity.compile_y == GRID_SIZE)
      state->entity.compiled = true;
    state->entity.compile_end = GetTickCount ();
  } 
  state->entity.compile_count++;


}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

bool EntityReady (state_t* state)
{

  return state->entity.compiled;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

float EntityProgress (state_t* state)
{

  return (float)state->entity.compile_count / (GRID_SIZE * GRID_SIZE);

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void EntityUpdate (state_t* state)
{
  
  unsigned stop_time;
  
  if (!TextureReady (state)) {
    state->entity.sorted = false;
    return;
  } 
  if (!state->entity.sorted) {
    qsort (state->entity.entity_list, state->entity.entity_count, sizeof (struct entity), do_compare);
    state->entity.sorted = true;
  }
  
  //We want to do several cells at once. Enough to get things done, but
  //not so many that the program is unresponsive.
  if (LOADING_SCREEN) {  //If we're using a loading screen, we want to build as fast as possible
    stop_time = GetTickCount () + 100;
    while (!state->entity.compiled && GetTickCount () < stop_time)
      do_compile (state);
  } else
    do_compile(state);
  
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void EntityRender (state_t* state)
{

  int       polymode[2];
  bool      wireframe;
  int       x, y;
  int       elapsed;

  //Draw all textured objects
  glGetIntegerv (GL_POLYGON_MODE, &polymode[0]);
  wireframe = polymode[0] != GL_FILL;
  if (RenderFlat (state))
    glDisable (GL_TEXTURE_2D);
  //If we're not using a loading screen, make the wireframe fade out via fog
  if (!LOADING_SCREEN && wireframe) {
    elapsed = 6000 - WorldSceneElapsed (state);
    if (elapsed >= 0 && elapsed <= 6000)
      RenderFogFX (state, (float)elapsed / 6000.0f);
    else
      return;
  }
  for (x = 0; x < GRID_SIZE; x++) {
    for (y = 0; y < GRID_SIZE; y++) {
      if (Visible (state, x,y))
        glCallList (state->entity.cell_list[x][y].list_textured);
    }
  }
  //draw all flat colored objects
  glBindTexture(GL_TEXTURE_2D, 0);
  glColor3f (0, 0, 0);
  for (x = 0; x < GRID_SIZE; x++) {
    for (y = 0; y < GRID_SIZE; y++) {
      if (Visible (state, x, y)) {
        if (wireframe)
          glCallList (state->entity.cell_list[x][y].list_flat_wireframe);
        else 
          glCallList (state->entity.cell_list[x][y].list_flat);
      }
    }
  }
  //draw all alpha-blended objects
  glBindTexture(GL_TEXTURE_2D, 0);
  glColor3f (0, 0, 0);
  glEnable (GL_BLEND);
  for (x = 0; x < GRID_SIZE; x++) {
    for (y = 0; y < GRID_SIZE; y++) {
      if (Visible (state, x,y)) {
        glCallList (state->entity.cell_list[x][y].list_alpha);
      }
    }
  }
  
}




/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void EntityClear (state_t* state)
{

  for (int i = 0; i < state->entity.entity_count; i++) {
    delete state->entity.entity_list[i].object;
  }
  if (state->entity.entity_list)
    free (state->entity.entity_list);
  state->entity.entity_list = NULL;
  state->entity.entity_count = 0;
  state->entity.compile_x = 0;
  state->entity.compile_y = 0;
  state->entity.compile_count = 0;
  state->entity.compiled = false;
  state->entity.sorted = false;

  int  x, y;

  for (x = 0; x < GRID_SIZE; x++) {
    for (y = 0; y < GRID_SIZE; y++) {
      glNewList (state->entity.cell_list[x][y].list_textured, GL_COMPILE);
      glEndList();	
      glNewList (state->entity.cell_list[x][y].list_alpha, GL_COMPILE);
      glEndList();	
      glNewList (state->entity.cell_list[x][y].list_flat_wireframe, GL_COMPILE);
      glEndList();	
      glNewList (state->entity.cell_list[x][y].list_flat, GL_COMPILE);
      glEndList();	
    }
  }


}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

int EntityCount (state_t* state)
{

  return state->entity.entity_count;

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

void EntityInit (state_t* state)
{

}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

int EntityPolyCount (state_t* state)
{

  if (!state->entity.sorted)
    return 0;
  if (state->entity.polycount)
    return state->entity.polycount;
  for (int i = 0; i < state->entity.entity_count; i++) 
    state->entity.polycount += state->entity.entity_list[i].object->PolyCount ();
  return state->entity.polycount;

}


/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

CEntity::CEntity (state_t* state)
{
  
  _state = state;
  add (_state, this);

}

void CEntity::Render (void)
{

}

void CEntity::RenderFlat (bool wireframe)
{

}

void CEntity::Update (void)
{


}



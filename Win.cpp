/*-----------------------------------------------------------------------------

  Win.cpp

  2006 Shamus Young

-------------------------------------------------------------------------------

  Create the main window and make it go.

-----------------------------------------------------------------------------*/


#ifdef __APPLE__

#import <Cocoa/Cocoa.h>

#include <time.h>

#include "Camera.h"
#include "Car.h"
#include "Entity.h"
#include "glTypes.h"
#include "Ini.h"
#include "Macro.h"
#include "Random.h"
#include "Render.h"
#include "Texture.h"
#include "Win.h"
#include "World.h"
#include "Visible.h"
#include "State.h"

int WinHeight(state_t* state)
{
  NSOpenGLView *view = (__bridge NSOpenGLView *)state->view;
  
  if (view.wantsBestResolutionOpenGLSurface)	// is HiDPI mode on?
  {
    if (view.window.screen)	// if so, use the backing scale factor from the window's screen
      return view.frame.size.height*view.window.screen.backingScaleFactor;
    else	// if we don't have a screen yet, we're going to have to wing it
      return view.frame.size.height*NSScreen.mainScreen.backingScaleFactor;
  }
  return view.frame.size.height;
}


int WinWidth(state_t* state)
{
  NSOpenGLView *view = (__bridge NSOpenGLView *)state->view;
  
  if (view.wantsBestResolutionOpenGLSurface)
  {
    if (view.window.screen)
      return view.frame.size.width*view.window.screen.backingScaleFactor;
    else
      return view.frame.size.width*NSScreen.mainScreen.backingScaleFactor;
  }
  return view.frame.size.width;
}


void AppInit (state_t* state)
{
  
  RandomInit (state, random());
  CameraInit (state);
  RenderInit (state);
  TextureInit (state);
  WorldInit (state);
  
}


void AppUpdate (state_t* state)
{
  CameraUpdate (state);
  EntityUpdate (state);
  WorldUpdate (state);
  TextureUpdate (state);
  VisibleUpdate (state);
  CarUpdate (state);
  RenderUpdate (state);
}


void AppTerm(state_t* state)
{
//  RandomInit (state, random());
  CameraTerm(state);
  RenderTerm(state);
  TextureTerm(state);
  WorldTerm(state);
  EntityClear(state);
}


#include <CoreServices/CoreServices.h>

uint64_t GetTickCount()
{
  uint64_t ticks = mach_absolute_time();
//  Nanoseconds nanos = AbsoluteToNanoseconds( *(AbsoluteTime *) &ticks );
//  return (* (uint64_t *) &nanos) / 1000*1000*1000*100;
  return ticks >> 21;
}

#endif


#include "State.h"


void VisibleUpdate (state_t* state);
bool Visible (state_t* state, GLvector pos);
bool Visible (state_t* state, int x, int z);

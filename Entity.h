#ifndef TYPES
#include "glTypes.h"
#endif

#ifndef ENTITY

#define ENTITY

#include "State.h"

class CEntity
{
private:
protected:

  GLvector                _center;
  state_t*                _state;

public:
                          CEntity (state_t* state);
  virtual                 ~CEntity () {};
  virtual void            Render (void);
  virtual void            RenderFlat (bool wirefame);
  virtual unsigned        Texture () { return 0; }
  virtual void            Update (void);
  virtual bool            Alpha () { return false; }
  virtual int             PolyCount () { return 0; }
  GLvector                Center () { return _center; }

};

void      EntityClear (state_t* state);
int       EntityCount (state_t* state);
float     EntityProgress (state_t* state);
bool      EntityReady (state_t* state);
void      EntityRender (state_t* state);
void      EntityUpdate (state_t* state);
int       EntityPolyCount (state_t* state);

#endif